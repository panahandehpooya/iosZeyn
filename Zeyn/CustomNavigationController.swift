//
//  CustomNavigationController.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 18/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit

// Classe criada unicamente para que status bar fosse setada como light (branca).
class CustomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

