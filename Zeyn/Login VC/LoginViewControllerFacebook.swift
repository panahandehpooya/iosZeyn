//
//  LoginViewControllerFacebook.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase

extension LoginViewController : FBSDKLoginButtonDelegate {
    
    // Facebook Login
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if let error = error {
            displayAlert(viewController: self, title: "Error", message: "Problem authenticating to Facebook. Try again later.\n Error: \(error.localizedDescription)")
            return
        }
        
        guard let token = FBSDKAccessToken.current() else {
            return
        }
        
        // Firebase Auth
        let credential = FacebookAuthProvider.credential(withAccessToken: token.tokenString)
        
        self.firebaseSignIn(credential: credential)
    }
    
    // Facebook logout
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        self.firebaseSignOut()
    }
}
