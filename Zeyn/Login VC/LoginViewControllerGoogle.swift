//
//  LoginViewControllerGoogle.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

extension LoginViewController : GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            self.displayAlert(viewController: self, title: "Error", message: "Problem authenticating to Google. Try again later.\n Error: \(error)")
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        self.firebaseSignIn(credential: credential)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        GIDSignIn.sharedInstance().disconnect()
        self.firebaseSignOut()
    }
}
