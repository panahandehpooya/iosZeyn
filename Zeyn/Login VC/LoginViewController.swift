//
//  LoginViewController.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 24/03/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Google sign-in
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func firebaseSignIn(credential: AuthCredential){
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                self.displayAlert(viewController: self, title: "Error", message: "Problem authenticating to Firebase. Try again later.\n Error: \(error)")
                return
            }
            
            // User signed in
            self.performSegue(withIdentifier: "loginSuccess", sender: nil)
        }
    }
    
    func firebaseSignOut(){
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            displayAlert(viewController: self, title: "Error", message: "Error while trying to log out. Details: \(signOutError.description)")
        }
    }
    
    func displayAlert(viewController: UIViewController, title: String, message: String){
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        controller.addAction(action)
        
        viewController.present(controller, animated: true, completion: nil)
    }

}
