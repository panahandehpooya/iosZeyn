//
//  MoreViewController.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit

class MoreViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0: // Logout
            LoginViewController().firebaseSignOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            self.present(controller, animated: true, completion: nil)
        case 1: // Go to Zeyn App website
            MoreViewController.openURL(link: "http://zeyn-todo.herokuapp.com", titleError: "Connection error", messageError: "It was not possible to connect to zeyn's website.", viewController: self)
        default:
            break
        }
    }
    
    static func openURL(link: String, titleError: String, messageError: String, viewController: UIViewController){
        guard let url = URL(string: link) else {
            let loginVC = LoginViewController()
            loginVC.displayAlert(viewController: viewController, title: titleError, message: messageError)
            
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
