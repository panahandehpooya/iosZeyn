//
//  TodoNote+CoreDataProperties.swift
//  
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//
//

import Foundation
import CoreData


extension TodoNote {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TodoNote> {
        return NSFetchRequest<TodoNote>(entityName: "TodoNote")
    }

    @NSManaged public var completed: Bool
    @NSManaged public var due: Date?
    @NSManaged public var owner: String?
    @NSManaged public var title: String?

}
