//
//  TodoCell.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit
import M13Checkbox

protocol TodoCellUpdater: class {
    func updateTableView()
}

class TodoCell: UITableViewCell {
    
    @IBOutlet weak var cellBG: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var checkBox: M13Checkbox!
    
    weak var delegate: TodoCellUpdater?
    
    @IBAction func checkBoxValueChanged(_ sender: M13Checkbox) {
        
        if (self.checkBox.checkState == .checked) {
            self.setCheckBoxChecked(animated: true)
        }else{
            self.setCheckBoxUnchecked(animated: true)
        }
        
        delegate?.updateTableView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCheckBoxChecked(animated: Bool){
        self.checkBox.setCheckState(.checked, animated: animated)
        self.cellBG.backgroundColor = UIColor(red: 224/256, green: 224/256, blue: 224/256, alpha: 1)
    }
    
    func setCheckBoxUnchecked(animated: Bool){
        self.checkBox.setCheckState(.unchecked, animated: animated)
        self.cellBG.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    }
}
