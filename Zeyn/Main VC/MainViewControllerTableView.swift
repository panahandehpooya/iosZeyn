//
//  MainViewControllerTableView.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit

extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoNotes_.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todoNote = self.todoNotes_[indexPath.row]
        
        self.performSegue(withIdentifier: "editSegue", sender: todoNote)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "todoCell") as? TodoCell else{return UITableViewCell()}
        let todoNote = self.todoNotes_[indexPath.row]
        
        cell.messageLabel.text = todoNote.title
        cell.delegate = self
        
        if todoNote.completed {
            cell.setCheckBoxChecked(animated: false)
        }else{
            cell.setCheckBoxUnchecked(animated: false)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete{
            let todoNote : TodoNote = self.todoNotes_[indexPath.row]
            
            let alert = UIAlertController(title: "Confirm", message: "Delete note \"\(todoNote.title ?? "")\" ?", preferredStyle: .alert)
            let confirm = UIAlertAction(title: "Yes", style: .default) { (result: UIAlertAction) -> Void in
                
                self.deleteNoteCoreData(todoNote: todoNote)
                self.todoNotes_.remove(at: self.todoNotes_.index(of: todoNote)!)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            let cancel = UIAlertAction(title: "No", style: .cancel)
            
            alert.addAction(cancel)
            alert.addAction(confirm)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func updateTableView() {
        self.mainTableview_.beginUpdates()
        self.mainTableview_.endUpdates()
    }
}
