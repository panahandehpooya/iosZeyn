//
//  CoreDataMain.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import CoreData

extension MainViewController{
    
    func getNotesCoreData(){
        let managedContext = self.getContext()
        
        let request : NSFetchRequest<TodoNote> = TodoNote.fetchRequest()
        
        do {
            self.todoNotes_ = try managedContext.fetch(request)
            self.todoNotes_.sort { (_, b) -> Bool in
                b.completed
            }
        } catch let error as NSError {
            fatalError("Não foi possível pegar os dados do CoreData. \(error), \(error.userInfo)")
        }
    }
    
    func noteToCoreData(completed: Bool, due: Date, owner: String, title: String?){
        let managedContext = self.getContext()
        
        let note = TodoNote(context: managedContext)
        note.completed = completed
        note.due = due
        note.owner = owner
        note.title = title
        
        do{
            try managedContext.save()
            self.todoNotes_.append(note);
        }catch let error as NSError {
            print("Não foi possível salvar. \(error), \(error.userInfo)")
        }
    }
    
    func deleteNoteCoreData(todoNote: TodoNote){
        
        let managedContext = self.getContext()
        
        managedContext.delete(todoNote)
        
        do{
            try managedContext.save()
        }catch let error as NSError {
            print("Não foi possível deletar. \(error), \(error.userInfo)")
        }
    }
}
