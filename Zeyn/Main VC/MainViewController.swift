//
//  MainViewController.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 24/03/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import M13Checkbox

class MainViewController: UIViewController, TodoCellUpdater {
    
    @IBOutlet var mainTableview_: UITableView!
    
    var todoNotes_ = [TodoNote]()
    
    func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.persistentContainer.viewContext
        return context!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getNotesCoreData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editSegue"{
            guard let editNodeVC = segue.destination as? EditNodeViewController,
                let sender_ = sender as? TodoNote else {
                    return
            }
            editNodeVC.todoNote_ = sender_
        }       
    }

}
