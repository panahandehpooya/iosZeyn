//
//  EditNodeViewController.swift
//  Zeyn
//
//  Created by Pedro Figueirêdo on 20/05/2018.
//  Copyright © 2018 ELTE. All rights reserved.
//

import UIKit

class EditNodeViewController: UIViewController {

    @IBOutlet weak var cellBG: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    
    var todoNote_ : TodoNote? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Checking todoNote variable
        if let todoNote = todoNote_ {
            self.messageTextView.text = todoNote.title
        }else{
            self.messageTextView.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveAlterations(_ sender: Any) {
    }
    
    @IBAction func closeVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
